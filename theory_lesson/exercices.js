// Exercice 1

/* Write the code to : 
Create an empty object called "user"
Add the porperty "name" with value your name
Add the property "surname" with value your surname
Change the property "name" to any other name
List the properties and values of "user" */

let user = {}; //Ici on a un objet vide

user["name"] = "Coline";
user["surname"] = "Baudrier";

// user.name = "Coline";
// user.surname = "Baudrier";

user.name = "Nube";

console.log(user);

for (let key in user) {
  console.log(`Next property ${key} with value ${user[key]}`);
}

//backquotes fait une chaîne de caractères
//${key} signifie que tu me mets la valeur actuelle de key
//${user[key]} va nous donner la valeur de key dans l'objet concerné

// Exercice 2

/* Nous avons le nombre de salariés d'une boîte :
let salaries = {
  pdg: 1,
  cadre: 10,
  ouvrier: 25
}

Calculer le nombre total de salariés */
let salaries = {
  pdg: 1,
  cadre: 10,
  ouvrier: 25,
};

console.log(salaries.pdg + salaries.cadre + salaries.ouvrier);
// Mais ça signifie qu'on voit l'intérieur de la variable

function combienDeSalaries(salaries) {
  let total = 0;
  let key; //retrouve les propriétés de l'objet
  for (key in salaries) {
    total = total + salaries[key];
  }
  console.log(total);
}

combienDeSalaries(salaries);

let employees = {};

for (key in salaries) {
  employees[key] = salaries[key];
}
console.log(employees);

// Exercice 3

let a = {
  who: { // premier niveau
    name: "Horacio", // un niveau plus bas
    surname: "Gonzalez",
  },
  where: "CESI",
};

// Let's clone Horacio

let b = a; // Is that a clone? NOT, same object

/*let c = {};
let key;
for (key in a) {
    c[key] = a[key];
}*/ //Ici on copie que le premier niveau mais on clone ceux en bas

// On peut utiliser la fonction assign aussi pour copier

c = structuredClone(a); // Ici on copie tous les niveaux


