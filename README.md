# lesson_horacio

## Introduction au JS
Rattrapage du cours d'Horacio de première année sur le JavaScript dans le développement Web

## Notions de cours
Pour faire du JS, on passe par la console : elle sert de moteur à JavaScript. 

- Alert permet d'envoyer un message à l'utilisateur
- Le point virgule est techniquement optionnel : JS est pensé pour des développeurs novices, le pré-compilateur va le rajouter à notre place si on l'oublie mais il vaut mieux y penser 
- console.log va permettre d'écrire le message directement dans la console (débuggage)
- let pour déclarer une variable : let NOMVAR = valeur
- Pour faire sauter le passage d'une valeur numérique en 64 bits, on met n à la fin du nombre qui est un équivalent BigInt (sert pour la précision mathématiques)
- typeof permet de renvoyer le type de l'opérateur avec la syntaxe : console.log(typeof OPERATEUR);
- On peut se servir des doubles ou des simples quotes mais il faut choisir pour tout le programme qu'on écrit (cohérence), on peut aussi utiliser les back quotes 
- Le string à back quotes permet de faire du multilignes et utiliser des variables avec $ pour l'appeler et d'autres caractères spéciaux
- Pour forcer une conversion en string d'un type différent : 
    - NOMVAR = string.(var);
- Pour faire de l'incrémentation : a = a + 1 peut être écrit a += 1 ou a = a++
- On peut déclarer ce type de variable : a = (b = valeur, c = valeur, b+c)
- Dans le code on met toujours du triple égal pour être sûr de faire une comparaison réelle, pas seulement de valeurs (égalité stricte)
- C'est une façon de coder un if else (42 === "42") ? console.log("Je code mal") : console.log("J'utilise des bonnes pratiques") en façon ternaire
- On peut utiliser ...params dans les paramètres d'une fonction pour lui indiquer qu'il y en a plusieurs qui sont attendus
- Timeout : permet de demander un appel de fonction différente au bout d'un moment (permet d'éviter l'asynchronie)
- C'est différent de setInterval qui va répéter la fonction avec un interval défini
- Prompt permet de récupérer une valeur tapée par l'utilisateur de la page 
- Quand on déclare une variable, la mémoire est structurée avec un espace de variables, un espace primitif, un espace pour les objets, donc si on copie et qu'on modifie ensuite, les deux variables sont modifiées
- Faire attention aux ID qu'on utilise dans le HTML pour ne pas qu'ils passent au dessus des fonctions JS 
- Quand on fait des fonctions, soit on met les évènements directement dans le HTML soit on met tout dans le JS