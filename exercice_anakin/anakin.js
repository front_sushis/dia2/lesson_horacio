let styleBasic = window.getComputedStyle(document.querySelector("#title")).color;
let altColor = "red";

function clickOnAnakin() {
  console.log("click on Anakin");
  alert("Click on Anakin");
}

//Version 4 ans
function clickOnName() {
  if (window.getComputedStyle(document.querySelector("#title")).color == styleBasic) {
    console.log("click on Name");
    document.querySelector("#title").style.color = altColor;
  } else {
    document.querySelector("#title").style.color = styleBasic;
  }
}

//Changement de la photo quand on la survole

let photoAnakin = document.querySelector("#photo").src;
let photoDarkVador = "https://www.radiofrance.fr/s3/cruiser-production/2020/12/5bcc195d-f284-40b9-b348-fde4155f6fcb/870x489_whatsapp_image_2020-12-11_at_15.00.43.jpg"

function darkVador() {
    document.querySelector("#photo").src = photoDarkVador;
}

function backOrigin () {
    document.querySelector("#photo").src = photoAnakin;
}

// On click sur les deux titres

function clickOnTitle() {
    document.querySelector("#title").style.color = "red";
}

function clickOnJob() {
    document.querySelector("#job").style.color = "red";
}

function clickElem(evt) {
    console.log(evt.srcElement);
}

//Permet de ne pas rajouter de JS dans l'HTML 
document.querySelector("#title").onclick = clickElem ;
document.querySelector("#job").onclick = clickElem ;

